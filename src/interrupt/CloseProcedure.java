/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interrupt;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import message.Request;
import message.RequestAbort;

/**
 *
 * @author Simão Paulo Rato Alves Reis
 * @version 1.0
 * @since 28-11-2012
 */
public class CloseProcedure extends Thread {

    private Request request;
    private InetAddress ipaddress, remoteipaddress;
    
    /**
     * 
     * @param ipaddress
     * @param request
     * @param remoteipaddress 
     */
    public CloseProcedure(InetAddress ipaddress, Request request, InetAddress remoteipaddress) {
        
        this.ipaddress = ipaddress;
        this.request = request;
        this.remoteipaddress = remoteipaddress;
    }
    
    /**
     * 
     */
    @Override
    public void run() {
        
        DatagramSocket udpSocket;
        DatagramPacket senderPacket;
        RequestAbort requestAbort;
        byte[] senderBuffer;

        try {
            requestAbort = new RequestAbort(ipaddress, request);
        } catch (UnknownHostException ex) {
            System.out.println(ex);
            return;
        }

        senderBuffer = requestAbort.toString().getBytes();

        try {
            udpSocket = new DatagramSocket();
        } catch (SocketException ex) {
            System.out.println(ex);
            return;
        }

        senderPacket = new DatagramPacket(senderBuffer, senderBuffer.length, remoteipaddress, 6800);
        try {
            udpSocket.send(senderPacket);
        } catch (IOException ex) {
            System.out.println(ex);
        }

        udpSocket.close();
    }
}
