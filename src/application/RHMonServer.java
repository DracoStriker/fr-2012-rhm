/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import address.IPAddress;
import exception.InvalidCommandException;
import exception.InvalidMessageException;
import exception.InvalidRequestErrorCodeException;
import exception.UknownOperatingSystem;
import exception.WrongPatternException;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.HashMap;
import message.Message;
import message.Ping;
import message.Pong;
import message.ProtocolMessageParser;
import message.Request;
import message.RequestAbort;
import message.RequestError;
import transport.TCPServer;

/**
 *
 * @author Rafael Saraiva Rigueiredo
 * @author Simão Paulo RAto Alves Reis
 * @version 1.2
 * @since 17-11-2012
 */
public class RHMonServer {

    private static InetAddress ipaddress;

    /**
     *
     * @param args Verificamos se o numero de argumentos é valido, se nao for,
     * imprimimos um Menu com o formato e opções. Se o args[0] não for um ip
     * duma interface local o programa tambem para.
     */
    public static void main(String... args) {

        byte[] receiverBuffer, senderBuffer;
        int senderPort;
        DatagramSocket udpSocket;
        DatagramPacket receiverPacket, senderPacket;
        String message;
        ProtocolMessageParser parser;
        Ping ping;
        Request request;
        InetAddress senderIP;
        RequestAbort requestAbort;
        HashMap<Integer, TCPServer> connectionList;
        TCPServer connection;

        if (args.length != 1) {
            getUsage();
            return;
        }

        try {
            ipaddress = InetAddress.getByName(args[0]);
        } catch (UnknownHostException ex) {
            System.out.println(ex);
            return;
        }

        if (!IPAddress.checkIPValid(ipaddress)) {
            System.out.println("Address in not Local.\n");
        }
        /*
         *  Socket UDP para receber pedidos.
         */
        try {
            udpSocket = new DatagramSocket(6800);
        } catch (SocketException ex) {
            System.out.println(ex);
            return;
        }

        parser = new ProtocolMessageParser();
        connectionList = new HashMap();

        while (true) {

            receiverBuffer = new byte[1500];
            receiverPacket = new DatagramPacket(receiverBuffer, receiverBuffer.length);
            
            /*
             * receber o pedido
             */
            
            try {
                udpSocket.receive(receiverPacket);
            } catch (IOException ex) {
                System.out.println(ex);
                continue;
            }

            receiverBuffer = receiverPacket.getData();
            senderIP = receiverPacket.getAddress();
            senderPort = receiverPacket.getPort();
            message = new String(receiverBuffer);
            System.out.println(message);
            
            /*
             * Processar a mensagem de pedido, se tiver erro de syntax verificamos se é um request
             * Se for, returnamos um requestError Code==1(SyntaxError).
             */
            try {
                parser.processString(message);
            } catch (InvalidMessageException | InvalidCommandException ex) {
                System.out.println(ex);
                continue;
            } catch (WrongPatternException ex) {
                if (parser.getProtocolMessageType() == Message.request) {
                    try {
                        request = (Request) parser.getProtocolMessage();
                    } catch (UnknownHostException | InvalidRequestErrorCodeException | InvalidMessageException e) {
                        System.out.println(ex);
                        continue;
                    }

                    try {
                        senderBuffer = new RequestError(ipaddress, request, 1).toString().getBytes();
                    } catch (UnknownHostException | InvalidRequestErrorCodeException ex1) {
                        System.out.println(ex);
                        continue;
                    }

                    senderPacket = new DatagramPacket(senderBuffer, senderBuffer.length, senderIP, senderPort);

                    try {
                        udpSocket.send(senderPacket);
                    } catch (IOException e) {
                        System.out.println(e);
                        continue;
                    }
                }
            }

            switch (parser.getProtocolMessageType()) {
                /*
                 * Se for um pacote Ping respondemos com Pong
                 */
                case ping:

                    try {
                        ping = (Ping) parser.getProtocolMessage();
                    } catch (UnknownHostException | InvalidRequestErrorCodeException | InvalidMessageException ex) {
                        System.out.println(ex);
                        break;
                    }

                    try {
                        senderBuffer = new Pong(ipaddress, ping).toString().getBytes();
                    } catch (UnknownHostException ex) {
                        System.out.println(ex);
                        break;
                    }

                    senderPacket = new DatagramPacket(senderBuffer, senderBuffer.length, senderIP, senderPort);

                    try {
                        udpSocket.send(senderPacket);
                    } catch (IOException ex) {
                        System.out.println(ex);
                        break;
                    }

                    break;

                case request:
                    
                    try {
                        request = (Request) parser.getProtocolMessage();
                    } catch (UnknownHostException | InvalidRequestErrorCodeException | InvalidMessageException ex) {
                        System.out.println(ex);
                        break;
                    }
                    /*
                     * Para cada request abrimos uma thread TCPServer que vai tratar da comunicação via TCP
                     */
                    try {
                        connection = new TCPServer(senderIP, request, senderPort);
                    } catch (IOException | InvalidCommandException | UknownOperatingSystem ex) {
                        System.out.println(ex);
                        break;
                    }
                    /*
                     * Adicionar a ligação TCP à lista de ligações, messageID enviado como argumento para sabermos sempre qual
                     * o número da ligação.
                     */
                    connectionList.put(request.getMessageID(), connection);
                    connection.start();
                    break;

                case requestabort:

                    try {
                        requestAbort = (RequestAbort) parser.getProtocolMessage();
                    } catch (UnknownHostException | InvalidRequestErrorCodeException | InvalidMessageException ex) {
                        System.out.println(ex);
                        break;
                    }

                    connection = connectionList.get(requestAbort.getMessageID());
                    /*
                     * Se recemos um RequestAbort, comparamos qual a comunicação pelo ID e acabamos a thread
                     * com a função connection.finish()
                     */
                    try {
                        connection.finish();
                        System.out.println("tcp connection aborted");
                    } catch (IOException ex) {
                        System.out.println(ex);
                    }

                    connectionList.remove(requestAbort.getMessageID());
            }
        }
    }

    private static void getUsage() {

        System.out.println("\nUsage:\nRHMonServer <IPAddress>");
    }
}
