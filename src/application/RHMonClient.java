/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import address.IPAddress;
import exception.InvalidCommandException;
import exception.InvalidMessageException;
import exception.WrongPatternException;
import file.InetAddressFile;
import interrupt.CloseProcedure;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import message.Command;
import message.Ping;
import message.ProtocolMessageParser;
import message.Request;
import transport.TCPClient;

/**
 *
 * @author Simão Paulo Rato Alves Reis
 * @author Rafael Saraiva Figueiredo
 * @version 1.0
 * @since 17-11-2012
 */
public class RHMonClient {

    private static InetAddress ipaddress, remoteipaddress;
    private static Command command;

    /**
     *
     * @param args Verificamos se o numero de argumentos é valido, se nao for,
     * imprimimos um Menu com o formato e opções. Se não for um ip valido
     * returnamos. Se o args[0] não for um ip duma interface local o programa
     * tambem para. Finalmente entramos num switch com três opções: list,
     * command ou opção invalida.
     */
    public static void main(String... args) {

        if (!(args.length == 2 || args.length == 3)) {
            getUsage();
            return;
        }

        try {
            ipaddress = InetAddress.getByName(args[0]);
        } catch (UnknownHostException ex) {
            System.out.println("Address in not Valid.\n");
            return;
        }

        if (!IPAddress.checkIPValid(ipaddress)) {
            System.out.println("Address in not Local.\n");
            return;
        }

        try {
            command = Command.fromString(args[1]);
        } catch (InvalidCommandException ex) {
            System.out.println(ex);
            return;
        }

        switch (command) {

            case list:

                if (args.length != 2) {
                    getUsage();
                }

                list();
                break;

            case conf:
            case route:
            case conn:

                if (args.length != 3) {
                    getUsage();
                }

                try {
                    remoteipaddress = InetAddress.getByName(args[2]);
                } catch (UnknownHostException ex) {
                    System.out.println(ex);
                    return;
                }

                comand(remoteipaddress, command);
                break;

            default:

                System.out.println("Invalid Command.\n");
        }
    }

    /**
     *
     * @return Função que imprime um Menu de auxilio ao utilizador.
     */
    private static void getUsage() {

        System.out.println("\nUsage:\nRHMonClient <IPAddress> <Command>\n"
                + "\nCommands:\nlist\n"
                + "conf <RemoteIPAddress>\n"
                + "route <RemoteIPAddress>\n"
                + "conn <RemoteIPAddress>");
    }

    /**
     * Função que trata da parte 3.1 e 3.3 no nosso programa. Está preparada
     * para mandar o PING em broadcast e PING em UNICAST caso existam PONGS em
     * falta em relação aos hosts previamente monotorizados.
     */
    private static void list() {

        DatagramSocket udpSocket;
        DatagramPacket receiverPacket, senderPacket;
        byte[] senderBuffer, receiverBuffer;
        Ping ping;
        InetAddressFile remoteList;
        ArrayList<InetAddress> nonReachedAddresses;
        /*
         * Criação de Socket UDP BroadCast com timeout de receive de 2 segundos.
         */
        try {
            udpSocket = new DatagramSocket();
            udpSocket.setBroadcast(true);
            udpSocket.setSoTimeout(2000);
        } catch (SocketException ex) {
            System.out.println(ex);
            return;
        }

        try {
            ping = new Ping(ipaddress);
        } catch (UnknownHostException ex) {
            System.out.println(ex);
            return;
        }

        senderBuffer = ping.toString().getBytes();

        try {
            senderPacket = new DatagramPacket(senderBuffer, senderBuffer.length, InetAddress.getByName("255.255.255.255"), 6800);
        } catch (UnknownHostException ex) {
            System.out.println(ex);
            return;
        }
        /*
         *Enviar os PINGS em broadcast.
         */
        try {
            udpSocket.send(senderPacket);
        } catch (IOException ex) {
            System.out.println(ex);
            return;
        }

        receiverBuffer = new byte[1500];
        receiverPacket = new DatagramPacket(receiverBuffer, receiverBuffer.length);
        remoteList = new InetAddressFile();
        /*
         * Ler o ficheiro, para pudermos comparar os PONGS recebidos e os hosts que foram previamente encontrados
         */
        try {
            remoteList.readFile(new File("./remotelist"));
        } catch (UnknownHostException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
            return;
        }
        /*
         * Receber todos os PONGS
         */
        while (true) {
            try {
                udpSocket.receive(receiverPacket);
                System.out.println(IPAddress.getByName(receiverPacket.getAddress()));
                remoteList.addInetAddress(receiverPacket.getAddress());
            } catch (SocketTimeoutException ex) {
                break;
            } catch (IOException ex) {
                System.out.println(ex);
            }
        }
        /*
         * Preparar a Socket UDP para enviar por UNICAST
         */
        try {
            udpSocket.setSoTimeout(1000);
            udpSocket.setBroadcast(false);
        } catch (SocketException ex) {
            System.out.println(ex);
            return;
        }
        /*
         * Array de InetAddresses que não responderam ao PONG e que estavam no ficheiro
         */
        nonReachedAddresses = remoteList.getNonReachedAddresses();

        /*
         * Re-enviar PING para esses ips por UNICAST
         */
        for (InetAddress address : nonReachedAddresses) {

            senderPacket = new DatagramPacket(senderBuffer, senderBuffer.length, address, 6800);

            try {
                udpSocket.send(senderPacket);
            } catch (IOException ex) {
                System.out.println(ex);
                continue;
            }

            receiverBuffer = new byte[1500];
            receiverPacket = new DatagramPacket(receiverBuffer, receiverBuffer.length);
            /*
             * Caso algum UNICAST responda com PONG adicionar ip ao ficheiro.
             */
            while (true) {
                try {
                    udpSocket.receive(receiverPacket);
                    remoteList.addInetAddress(receiverPacket.getAddress());
                    System.out.println(IPAddress.getByName(receiverPacket.getAddress()));
                } catch (SocketTimeoutException ex) {
                    break;
                } catch (IOException ex) {
                    System.out.println(ex);
                }
            }
        }
             /*
             * Escrever no ficheiro
             */
        try {
            remoteList.writeFile();
        } catch (UnknownHostException | FileNotFoundException ex) {
            System.out.println(ex);
        }

        udpSocket.close();
    }

    /**
     *
     * @param remoteipaddress
     * @param comand
     * Função que trata da 3.2 e 3.4 do nosso programa. Responsavel pelas comunicações
     * TCP. Envia um Request com o comando e cria uma serverSocket que fica à espera que o servidor
     * responda.
     */
    private static void comand(InetAddress remoteipaddress, Command comand) {

        DatagramSocket datagramSocket;
        DatagramPacket packet;
        byte[] buffer;
        ServerSocket server;
        Request request;
        InetAddressFile remoteList;
        String message;
        CloseProcedure cp;

        remoteList = new InetAddressFile();
             /*
             * Ler o ficheiro de ips
             */
        try {
            remoteList.readFile(new File("./remotelist"));
        } catch (UnknownHostException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
            return;
        }
            /*
             * Verificar se o ip para o qual pretendemos comunicar está no ficheiro. Se não estiver, termina. 
             */
        if (!remoteList.addressExists(remoteipaddress)) {
            System.out.println("Remote Host not known.\n");
            return;
        }
            /*
             *  Criar serverSocket. Porta criada aleatoriamente pelo SO. 
             */
        try {
            server = new ServerSocket(0, 0, ipaddress);
        } catch (IOException ex) {
            System.out.println(ex);
            return;
        }
             /*
             * Socket UDP para enviar e receber Request e Request Done/Error
             */
        try {
            datagramSocket = new DatagramSocket();
        } catch (SocketException ex) {
            System.out.println(ex);
            return;
        }

        try {
            request = new Request(ipaddress, server.getLocalPort(), comand);
        } catch (UnknownHostException ex) {
            System.out.println(ex);
            return;
        }

        buffer = request.toString().getBytes();
        packet = new DatagramPacket(buffer, buffer.length, remoteipaddress, 6800);
             /*
             * Thread que vai tratar da parte TCP no cliente.
             */
        new TCPClient(server).start();

        try {
            datagramSocket.setSoTimeout(3000);
        } catch (SocketException ex) {
            System.out.println(ex);
        }

        Runtime.getRuntime().addShutdownHook(cp = new CloseProcedure(ipaddress, request, remoteipaddress));
        
        
            /*
             * Enviar o Request e adicionada a Hook, que caso precionado Control-C envia um requestAbort para o servidor.
             */
        try {
            datagramSocket.send(packet);
        } catch (IOException ex) {
            System.out.println(ex);
        }

        buffer = new byte[1500];
        packet = new DatagramPacket(buffer, buffer.length);
        
             /*
             * Caso a ligação tcp não seja iniciada e não seja recebido requestError/Done, faz sentido que o request não 
             * tenha chegado ao servidor, logo, o request é re-enviado, até um maximo de 3 re-envios.
             */
        for (int counter = 0; counter < 3; counter++) {

            try {
                datagramSocket.receive(packet);
                break;

            } catch (IOException ex) {

                System.out.println(ex);

                if (TCPClient.getConnectionStatus()) {
                    break;
                } else {
                    buffer = request.toString().getBytes();
                    packet = new DatagramPacket(buffer, buffer.length, remoteipaddress, 6800);
                    new TCPClient(server).start();

                    try {
                        datagramSocket.send(packet);
                        System.out.println("request re-enviado");
                    } catch (IOException ex1) {
                        System.out.println(ex1);
                        continue;
                    }

                    buffer = new byte[1500];
                    packet = new DatagramPacket(buffer, buffer.length);
                }
            }
        }
        
        
             /*
             *  A comunicação por TCP/UDP ja terminou, não faz sentido continuarmos com o hook logo removemos,
             * para terminar vamos processar a resposta UDP e fechar as Sockets.
             */
        Runtime.getRuntime().removeShutdownHook(cp);
        message = new String(packet.getData());
        System.out.println(message);
        ProtocolMessageParser parser = new ProtocolMessageParser();

        try {

            parser.processString(message);

        } catch (InvalidMessageException | WrongPatternException | InvalidCommandException ex) {

            System.out.println(ex);
            datagramSocket.close();

            try {
                server.close();
            } catch (IOException ex1) {
                System.out.println(ex1);
            }

            return;
        }

        switch (parser.getProtocolMessageType()) {

            case requestdone:

                datagramSocket.close();

                try {
                    server.close();
                } catch (IOException ex) {
                    System.out.println(ex);
                }

                System.out.println("Request Done");
                break;

            case requesterror:

                datagramSocket.close();

                try {
                    server.close();
                } catch (IOException ex) {
                    System.out.println(ex);
                }

                System.out.println("Request Error");
        }
    }
}
