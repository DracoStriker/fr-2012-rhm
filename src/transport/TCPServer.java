/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package transport;

import exception.InvalidCommandException;
import exception.InvalidRequestErrorCodeException;
import exception.UknownOperatingSystem;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import message.Command;
import message.Request;
import message.RequestDone;
import message.RequestError;

/**
 *
 * @author Simão Paulo Rato Alves Reis
 * @author Rafael Saraiva Figueiredo
 * @version 1.0
 * @since 30-11-2012
 */
public class TCPServer extends Thread {

    private String command;
    private Socket tcpSocket;
    private InetAddress senderIP;
    private Request request;
    private int senderPort;
    
    /**
     * 
     * @param address
     * @param request
     * @param senderPort
     * @throws IOException
     * @throws InvalidCommandException
     * @throws UknownOperatingSystem 
     */
    public TCPServer(InetAddress address, Request request, int senderPort) throws IOException, InvalidCommandException, UknownOperatingSystem {

        this.senderIP = address;
        this.senderPort = senderPort;
        this.request = request;
        this.command = getSystemCommand(request.getRequestType());
        tcpSocket = new Socket(address, request.getPortNumber());
    }

    @Override
    public void run() {

        Process process;
        BufferedReader bufferReader;
        BufferedWriter bufferWriter;
        String line;
        DatagramSocket udpSocket;
        DatagramPacket senderPacket;
        byte[] senderBuffer;
        RequestDone requestDone;
        RequestError requestError;
        /*
         * Criar o BufferedWriter e o processo para a leitura da informação do sistema.
         */
        try {
            bufferWriter = new BufferedWriter(new OutputStreamWriter(tcpSocket.getOutputStream()));
            process = Runtime.getRuntime().exec(command);
            bufferReader = new BufferedReader(new InputStreamReader(process.getInputStream(), "Cp850"));
            /*
             * Enquanto houver informação(!=null) enviar pelo BufferedWriter
             */
            while ((line = bufferReader.readLine()) != null) {
                bufferWriter.write(line);
                bufferWriter.newLine();
                bufferWriter.flush();
            }

        } catch (IOException ex) {
            /*
             * Se existir um problema de IO enviar um requestError com Code == 2
             * que significa que a ligação TCP teve problemas.
             */
            try {
                requestError = new RequestError(senderIP, request, 2);
            } catch (InvalidRequestErrorCodeException | UnknownHostException ex1) {
                System.out.println(ex1);
                return;
            }

            try {
                udpSocket = new DatagramSocket();
            } catch (SocketException ex1) {
                System.out.println(ex);
                return;
            }

            senderBuffer = requestError.toString().getBytes();
            senderPacket = new DatagramPacket(senderBuffer, senderBuffer.length, senderIP, senderPort);

            try {
                udpSocket.send(senderPacket);
            } catch (IOException ex1) {
                System.out.println(ex1);
                return;
            }

            udpSocket.close();
        }

        try {
            tcpSocket.close();
        } catch (IOException ex) {
            System.out.println(ex);
            return;
        }
        /*
         * Se correu tudo bem retorna um requestDone
         */
        try {
            requestDone = new RequestDone(senderIP, request);
        } catch (UnknownHostException ex) {
            System.out.println(ex);
            return;
        }

        try {
            udpSocket = new DatagramSocket();
        } catch (SocketException ex) {
            System.out.println(ex);
            return;
        }
        
        senderBuffer = requestDone.toString().getBytes();
        senderPacket = new DatagramPacket(senderBuffer, senderBuffer.length, senderIP, senderPort);

        try {
            udpSocket.send(senderPacket);
        } catch (IOException ex) {
            System.out.println(ex);
        }

        udpSocket.close();
    }
    
    /**
     * 
     * @throws IOException 
     * Termina a ligação TCP
     */
    public void finish() throws IOException {

        tcpSocket.close();
    }
    
    /**
     * 
     * @param command
     * @return
     * @throws InvalidCommandException
     * @throws UknownOperatingSystem 
     * Menu com as opções de comandos para Windows e Linux
     */
    private String getSystemCommand(Command command) throws InvalidCommandException, UknownOperatingSystem {

        String operatingSystem = System.getProperty("os.name").split(" ")[0];

        switch (operatingSystem) {

            case "Windows":

                switch (command) {

                    case conf:

                        return "ipconfig";

                    case route:

                        return "route print";

                    case conn:

                        return "netstat -n";

                    default:

                        throw new InvalidCommandException();
                }

            case "Linux":

                switch (command) {

                    case conf:

                        return "ifconfig";

                    case route:

                        return "route -n";

                    case conn:

                        return "netstat -n";

                    default:

                        throw new InvalidCommandException();
                }

            default:

                throw new UknownOperatingSystem();
        }
    }
}
