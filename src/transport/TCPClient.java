/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package transport;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 *
 * @author Simão Paulo Rato Alves Reis
 * @author Rafael Saraiva Figueiredo
 * @version 1.0
 * @since 04-12-2012
 */
public class TCPClient extends Thread {
    
    private ServerSocket serverSocket;
    private static boolean connection;
    
    /**
     * 
     * @param serverSocket 
     */
    public TCPClient(ServerSocket serverSocket) {
        
        this.serverSocket = serverSocket;
        connection = false;
    }

    @Override
    public void run() {

        Socket tcpSocket;
        BufferedReader in;
        String temp;
        
        /*
         * Aceitar Ligação
         */
        try {
            tcpSocket = serverSocket.accept();
        } catch (IOException ex) {
            System.out.println(ex);
            return;
        }
        /*
         * Conseguimos aceitar uma ligação, logo, connection = true;
         */
        connection = true;
        
        try {
            tcpSocket.setSoTimeout(3000);
        } catch (SocketException ex) {
            System.out.println(ex);
            return;
        }
        
        try {
            in = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream()));
        } catch (IOException ex) {
            System.out.println(ex);
            return;
        }
        
        try {
            while ((temp = in.readLine()) != null) {
                System.out.println(temp);
            }
        } catch (IOException ex) {
            System.out.println(ex);
        }
        
        try {
            tcpSocket.close();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
    
    /**
     * 
     * @return 
     */
    public static boolean getConnectionStatus() {
        
        return connection;
    }
}
