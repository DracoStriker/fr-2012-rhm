/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package message;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author Simão Paulo Rato Alves Reis
 * @version 1.0
 * @since 17-11-2012
 */
public final class Request extends ProtocolMessage {

    private int portnumber;
    private Command type;
    
    /**
     * 
     * @param host
     * @param portnumber
     * @param type
     * @throws UnknownHostException 
     */
    public Request(InetAddress host, int portnumber, Command type) throws UnknownHostException {
        
        super(host);
        this.portnumber = portnumber;
        this.type = type;
    }
    
    /**
     * 
     * @param host
     * @param messageid
     * @param portnumber
     * @param type
     * @throws UnknownHostException 
     */
    Request(InetAddress host, int messageid, int portnumber, Command type) throws UnknownHostException {
        
        super(host, messageid);
        this.portnumber = portnumber;
        this.type = type;
    }
    
    @Override
    public String toString() {
        
        return "REQUEST\n" + super.toString() + "PORT: " + this.portnumber + "\nTYPE: " + this.type + "\n";
    }
    
    /**
     * 
     * @return 
     */
    public int getPortNumber() {
        
        return portnumber;
    }
    
    /**
     * 
     * @return 
     */
    public Command getRequestType() {
        
        return type;
    }
}
