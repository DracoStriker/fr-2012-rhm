/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package message;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author Simão Paulo Rato Alves Reis
 * @version 1.0
 * @since 17-11-2012
 */
public final class RequestAbort extends ProtocolMessage {
    
    /**
     * 
     * @param host
     * @param request
     * @throws UnknownHostException 
     */
    public RequestAbort(InetAddress host, Request request) throws UnknownHostException {

        super(host, request);
    }
    
    /**
     * 
     * @param host
     * @param messageid 
     */
    RequestAbort(InetAddress host, int messageid) {

        super(host, messageid);
    }
    
    @Override
    public String toString() {

        return "REQUEST_ABORT\n" + super.toString();
    }
}
