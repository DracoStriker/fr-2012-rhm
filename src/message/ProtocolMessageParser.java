/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package message;

import exception.InvalidCommandException;
import exception.InvalidMessageException;
import exception.InvalidRequestErrorCodeException;
import exception.WrongPatternException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author Simão Paulo Rato Alves Reis
 * @version 1.0
 * @since 19-11-2012
 */
public class ProtocolMessageParser {

    private Message messagetype;
    private InetAddress host;
    private Command type;
    private int messageid, portnumber, code;
    
    /**
     * 
     * @param string
     * @throws InvalidMessageException
     * @throws WrongPatternException
     * @throws InvalidCommandException 
     */
    public void processString(String string) throws InvalidMessageException, WrongPatternException, InvalidCommandException {

        String[] fields = string.split("[ \n]");
        messagetype = Message.fromString(fields[0]);

        if (fields[1].compareTo("HOST:") != 0) {
            throw new WrongPatternException();
        }

        try {
            host = InetAddress.getByName(fields[2]);
        } catch (UnknownHostException ex) {
            System.out.println(ex);
        }

        if (fields[3].compareTo("MESSAGEID:") != 0) {
            throw new WrongPatternException();
        }

        messageid = Integer.parseInt(fields[4]);
        
        switch (messagetype) {

            case request:

                if (fields[5].compareTo("PORT:") != 0) {
                    throw new WrongPatternException();
                }
                
                portnumber = Integer.parseInt(fields[6]);

                if (fields[7].compareTo("TYPE:") != 0) {
                    throw new WrongPatternException();
                }

                type = Command.fromString(fields[8]);
                break;

            case requestdone:

                if (fields[5].compareTo("TYPE:") != 0) {
                    throw new WrongPatternException();
                }

                type = Command.fromString(fields[6]);
                break;

            case requesterror:

                if (fields[5].compareTo("TYPE:") != 0) {
                    throw new WrongPatternException();
                }

                type = Command.fromString(fields[6]);

                if (fields[7].compareTo("CODE:") != 0) {
                    throw new WrongPatternException();
                }

                code = Integer.parseInt(fields[8]);
        }
    }
    
    /**
     * 
     * @return 
     */
    public Message getProtocolMessageType() {

        return messagetype;
    }
    
    /**
     * 
     * @return 
     */
    public int getMessageID() {

        return messageid;
    }
    
    /**
     * 
     * @return 
     */
    public int getPortNumber() {
        
        return portnumber;
    }
    
    /**
     * 
     * @return 
     */
    public InetAddress getHostAddress() {
        
        return host;
    }
    
    /**
     * 
     * @return
     * @throws UnknownHostException
     * @throws InvalidRequestErrorCodeException
     * @throws InvalidMessageException 
     */
    public ProtocolMessage getProtocolMessage() throws UnknownHostException, InvalidRequestErrorCodeException, InvalidMessageException {

        switch (messagetype) {

            case ping:
                
                return new Ping(host, messageid);

            case pong:

                return new Pong(host, messageid);

            case request:

                return new Request(host, messageid, portnumber, type);

            case requestdone:

                return new RequestDone(host, messageid, type);

            case requesterror:

                return new RequestError(host, messageid, type, code);

            case requestabort:

                return new RequestAbort(host, messageid);

            default:

                throw new InvalidMessageException();
        }
    }
}
