/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package message;

import address.IPAddress;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;

/**
 *
 * @author Simão Paulo Rato Alves Reis
 * @version 1.0
 * @since 17-11-2012
 */
public abstract class ProtocolMessage {

    private InetAddress host;
    private int messageid;
    
    /**
     * 
     * @param host
     * @throws UnknownHostException 
     */
    ProtocolMessage(InetAddress host) throws UnknownHostException {
        
        this.host = host;
        messageid = Math.abs(new Random().nextInt());
    }
    
    /**
     * 
     * @param host
     * @param protocolmessage
     * @throws UnknownHostException 
     */
    ProtocolMessage(InetAddress host, ProtocolMessage protocolmessage) throws UnknownHostException {
        
        this.host = host;
        messageid = protocolmessage.messageid;
    }
    
    /**
     * 
     * @param host
     * @param messageid 
     */
    ProtocolMessage(InetAddress host, int messageid) {
        
        this.host = host;
        this.messageid = messageid;
    }
    
    @Override
    public String toString() {
        
        return "HOST: " + IPAddress.getByName(host) + "\nMESSAGEID: " + messageid + "\n";
    }
    
    /**
     * 
     * @return 
     */
    public InetAddress getHost() {
        
        return host;
    }
    
    /**
     * 
     * @return 
     */
    public int getMessageID() {
        
        return messageid;
    }
}
