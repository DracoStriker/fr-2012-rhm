/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package message;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 
 * @author Simão Paulo Rato Alves Reis
 * @version 1.0
 * @since 17-11-2012
 */
public class Ping extends ProtocolMessage {
    
    /**
     * 
     * @param host
     * @throws UnknownHostException 
     */
    public Ping(InetAddress host) throws UnknownHostException {

        super(host);
    }
    
    /**
     * 
     * @param host
     * @param messageid 
     */
    Ping(InetAddress host, int messageid) {

        super(host, messageid);
    }
    
    @Override
    public String toString() {

        return "PING\n" + super.toString();
    }
}
