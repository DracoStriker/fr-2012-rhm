/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package message;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author Simão Paulo Rato Alves Reis
 * @version 1.0
 * @since 17-11-2012
 */
public final class RequestDone extends ProtocolMessage {

    private Command type;
    
    /**
     * 
     * @param host
     * @param request
     * @throws UnknownHostException 
     */
    public RequestDone(InetAddress host, Request request) throws UnknownHostException {

        super(host, request);
        this.type = request.getRequestType();
    }
    
    /**
     * 
     * @param host
     * @param messageid
     * @param type
     * @throws UnknownHostException 
     */
    RequestDone(InetAddress host, int messageid, Command type) throws UnknownHostException {

        super(host, messageid);
        this.type = type;
    }
    
    @Override
    public String toString() {

        return "REQUEST_DONE\n" + super.toString() + "TYPE: " + this.type + "\n";
    }
    
    /**
     * 
     * @return 
     */
    public Command getType() {

        return type;
    }
}
