/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package message;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author Simão Paulo Rato Alves Reis
 * @version 1.0
 * @since 17-11-2012
 */
public class Pong extends ProtocolMessage {
    
    /**
     * 
     * @param host
     * @param ping
     * @throws UnknownHostException 
     */
    public Pong(InetAddress host, Ping ping) throws UnknownHostException {

        super(host, ping);
    }
    
    /**
     * 
     * @param host
     * @param messageid 
     */
    Pong(InetAddress host, int messageid) {

        super(host, messageid);
    }
    
    @Override
    public String toString() {

        return "PONG\n" + super.toString();
    }
}
