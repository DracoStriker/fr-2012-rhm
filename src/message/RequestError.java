/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package message;

import exception.InvalidRequestErrorCodeException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author Simão Paulo Rato Alves Reis
 * @version 1.0
 * @since 17-11-2012
 */
public final class RequestError extends ProtocolMessage {

    private Command type;
    private int code;
    
    /**
     * 
     * @param host
     * @param request
     * @param code
     * @throws InvalidRequestErrorCodeException
     * @throws UnknownHostException 
     */
    public RequestError(InetAddress host, Request request, int code) throws InvalidRequestErrorCodeException, UnknownHostException {

        super(host, request);
        type = request.getRequestType();
        this.code = code;
        validRequestErrorCode();
    }
    
    /**
     * 
     * @param host
     * @param messageid
     * @param type
     * @param code
     * @throws InvalidRequestErrorCodeException 
     */
    RequestError(InetAddress host, int messageid, Command type, int code) throws InvalidRequestErrorCodeException {

        super(host, messageid);
        this.type = type;
        this.code = code;

        validRequestErrorCode();
    }

    @Override
    public String toString() {

        return "REQUEST_ERROR\n" + super.toString() + "TYPE: " + this.type + "\nCODE: " + this.code + "\n";
    }
    
    /**
     * 
     * @return 
     */
    public Command getRequestType() {

        return type;
    }
    
    /**
     * 
     * @return 
     */
    public int getCode() {

        return code;
    }
    
    /**
     * 
     * @throws InvalidRequestErrorCodeException 
     */
    private void validRequestErrorCode() throws InvalidRequestErrorCodeException {

        if (code != 1 && code != 2) {
            throw new InvalidRequestErrorCodeException();
        }
    }
}
