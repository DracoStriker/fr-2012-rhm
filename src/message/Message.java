/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package message;

import exception.InvalidMessageException;

/**
 * 
 * @author Simão Paulo Rato Alves Reis
 * @version 1.0
 * @since 21-11-2012
 */
public enum Message {

    ping {
        @Override
        public String toString() {
            return "PING";
        }
    }, pong {
        @Override
        public String toString() {
            return "PONG";
        }
    }, request {
        @Override
        public String toString() {
            return "REQUEST";
        }
    }, requestabort {
        @Override
        public String toString() {
            return "REQUEST_ABORT";
        }
    }, requestdone {
        @Override
        public String toString() {
            return "REQUEST_DONE";
        }
    }, requesterror {
        @Override
        public String toString() {
            return "REQUEST_ERROR";
        }
    };
    
    /**
     * 
     * @param string
     * @return
     * @throws InvalidMessageException 
     */
    public static Message fromString(String string) throws InvalidMessageException {

        switch (string) {

            case "PING":

                return ping;

            case "PONG":

                return pong;

            case "REQUEST":

                return request;

            case "REQUEST_ABORT":

                return requestabort;

            case "REQUEST_DONE":

                return requestdone;

            case "REQUEST_ERROR":

                return requesterror;

            default:

                throw new InvalidMessageException();
        }
    }
}
