/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package file;

import address.IPAddress;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Simão Paulo Rato Alves Reis
 * @author Rafael Saraiva Figueiredo
 * @version 1.0
 * @since 28-11-2012
 */
public class InetAddressFile {

    private ArrayList<InetAddress> newList, fileList;
    private File file;
    
    /**
     * 
     */
    public InetAddressFile() {
        
        newList = new ArrayList<>();
    }
    
    /**
     * 
     * @param ipaddress 
     */
    public void addInetAddress(InetAddress ipaddress) {

        if (!addressExists(ipaddress, newList)) {
            newList.add(ipaddress);
        }
    }
    
    /**
     * 
     * @param file
     * @throws FileNotFoundException
     * @throws UnknownHostException
     * @throws IOException 
     */
    public void readFile(File file) throws FileNotFoundException, UnknownHostException, IOException {

        Scanner sc;
        
        fileList = new ArrayList<>();
        
        if (!file.exists()) {
            file.createNewFile();
        }

        sc = new Scanner(this.file = file);
        
        while (sc.hasNextLine()) {
            fileList.add(InetAddress.getByName(sc.nextLine()));
        }

        sc.close();
    }
    
    /**
     * 
     * @throws FileNotFoundException
     * @throws UnknownHostException 
     */
    public void writeFile() throws FileNotFoundException, UnknownHostException {

        PrintWriter pw = new PrintWriter(file);

        for (InetAddress address : newList) {
            pw.println(IPAddress.getByName(address));
        }

        pw.close();
    }
    
    /**
     * 
     * @return 
     */
    public ArrayList<InetAddress> getNonReachedAddresses() {

        ArrayList<InetAddress> resentList = new ArrayList<>();

        for (InetAddress address : fileList) {
            if (!addressExists(address, newList)) {
                resentList.add(address);
            }
        }

        return resentList;
    }
    
    /**
     * 
     * @param ipaddress
     * @param addressList
     * @return 
     */
    private boolean addressExists(InetAddress ipaddress, ArrayList<InetAddress> addressList) {

        for (InetAddress address : addressList) {
            if (address.equals(ipaddress)) {
                return true;
            }
        }

        return false;
    }
    
    /**
     * 
     * @param ipaddress
     * @return 
     */
    public boolean addressExists(InetAddress ipaddress) {

        return addressExists(ipaddress, fileList);
    }
}
