/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package address;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;

/**
 *
 * @author Simão Paulo Rato Alves Reis
 * @author Rafael Saraiva Figueiredo
 * @version 1.0
 * @since 28-11-2012
 */
public class IPAddress {
    
    /**
     * 
     * @param ipaddress
     * @return 
     */
    public static String getByName(InetAddress ipaddress) {

        byte[]  bytes;
        
        bytes = ipaddress.getAddress();

        return (bytes[0] & 255) + "."
                + (bytes[1] & 255) + "."
                + (bytes[2] & 255) + "."
                + (bytes[3] & 255);
    }
    
    /**
     * 
     * @param ipaddress
     * @return 
     */
    public static boolean checkIPValid(InetAddress ipaddress) {
        
        if (ipaddress.isAnyLocalAddress()) {
            
            return true;
        }
        
        try {
            
            return NetworkInterface.getByInetAddress(ipaddress) != null;
            
        } catch (SocketException ex) {
            System.out.println(ex);
            return false;
        }
    }
    
    /**
     * 
     * @param ipaddress
     * @return 
     */
    public static String getLocalBroadCast(InetAddress ipaddress) {
        
        byte[] bytes;

        bytes = ipaddress.getAddress();

        return (bytes[0] & 255) + "."
                + (bytes[1] & 255) + "."
                + (bytes[2] & 255) + "."
                + "255";
    }
}
