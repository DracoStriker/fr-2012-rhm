3th Year Course - Network Fundamentals
-------------------------------------------

Project Members
---------------
Simão Reis 

Rafael Saraiva

Project Description
-------------------
This  application  provides  a remote host  monitoring  service  and  has  a  client-server architecture.  The  client  application  enables  the  user  (i)  to  identify  the list  of IP hosts  running  the server  application  and  (ii)  to  request  information  on  their  IP  configuration or status.  The  server application  is  always-on  and  replies  to  client  requests  with  the  requested  information (the  server application does not require user interaction).

The  information  to  be  provided,  on  request,  by  the  server  application  is  the  host  IP  configuration (output  of ipconfig command  on  Windows  OS or ifconfig command  on  Linux  OS),  the  host  IP  routing table (output of route print command on Windows OS or route -ncommand on Linux OS) and the host list of active connections (output of netstat -ncommand on both Windows OS and Linux OS).

A  monitoring enabled  host  is  an  IP  host  running  the  server  application. The  identification  of  all monitoring  enabled  hosts  runs  over  UDP protocol.  The  information  requests  sent  by  clients also run over  UDP protocol.  The content (with the  requested information) sent  by  servers to  clients run  over TPCprotocol. Server UDP port number is 6800. Client UDP port number and TCP port numbers (both client and server) are dynamic (they are assigned by Operating System).

Identification of monitoring enabled hosts
------------------------------------------
The  identification  of  monitoring  enabled  hosts  is  activated  by  user  request  on  the  client  application. This action involves the PING and PONG messages (both sent over UDP). Every time that users request this actionwith the list command:

a) the  client  application sends one PING  message  to  the  broadcast  address identifying  its  own  IP address;
b) each active server replies to the IP address of the requester with a PONG message identifying its IP address;
c) the  client  application  waits  for  all  PONG  messages  until  it  does  not  receive  any  message  for  2 seconds.

Request of information
----------------------
The  request  of  information  is  activated  by  user  request  on  the  client  application. Information  can  be requested  only  to  monitoring  enabled  hosts  that  were previously identified. This  actioninvolves  the REQUEST, REQUEST_DONE and REQUEST_ERROR messages (all sent over UDP). Every time that users request information:

1) the client application opens a TCP socket (to receive the requested information) and sends to the remote  host  a  REQUEST  message  identifying thedesired information and  the  port  number  of  its TCP socket;
2) if the server application detects a syntax error on the received REQUEST message, it sends to the IP address of the requester a REQUEST_ERROR message reporting this error situation; otherwise, the server application tries to establish a TCP connection to the client application;
3) if the  TCP  connection  is  established,  the  server  application (i) sends  the  requested  information through  the  TCP  connection, (ii) ends  the  TCP  connection and  (iii)  sends  to  the  IP  address  of  the requester a REQUEST_DONE message acknowledging the request completion;
4) if  the  TCP  connection is  not established, the  server  application  sends  to  the  IP  address  of  the requester a REQUEST_ERROR message reporting this error situation.

Enhanced identification of monitoring enabled hosts
---------------------------------------------------
Since PING and PONG messages use the unreliable UDP service, there is no guarantee that they reachtheir destinations. In order to improve the service reliability, the client application maintains a list of known monitoring enabled hosts (saved on a file). Then, every time that users request this actionwith the list command: 

d) for every monitoringenabled host that was previously inthe list and the client application didnot receive the PONG message, the client application sends one PING message to its IP address;
e) the active server with the destination IP address of the PING message replies to the IP address of the requester with a PONG message identifying its IP address;
f) the client application waits for each PONG message for 1 second.

Steps  d) to f) aim  to minimize  the probability of a previously known active server being deleted from the client list due to failure of the first exchange of PING and PONG messages in steps a) to c).

Reliable request of information
-------------------------------
Since  REQUEST, REQUEST_DONE and  REQUEST_ERROR messages  use  the  unreliable  UDP  service, there  is  no  guarantee  that  they reach their  destinations.  In  order  to  make  reliable  the  information request process, the following action must be included.

* If  the  client  application sends  a  REQUEST  message but  no  TCP  connection is established andno REQUEST_ERROR  message  is  received from the  server application, the  REQUEST  message  is retransmitted (timeout of 3 seconds and a maximum of 3 transmissions).

Abort mechanism
---------------
During  the  request  of  information,  the  user  might  want  to  cancel  the  action  (for  example,  keying Ctlr+C).  In  this  case,  the  TCP  connection  must  be  aborted  and  a  REQUEST_ABORT  message  must  be sent by the client application to the server application. Upon reception of a REQUEST_ABORT message, the server application must cancel the processing of the request but no reply message is required.

Server capacity
---------------
In its basic version, the server application deals at most with one request at a time. In this version, the server application implementation is easier (since it does not need to have more than one established TCP  connection) but the server has  worse  performance  (the server response  time is  degraded  when the incoming requests rate grows).

In its enhanced version, the server application deals simultaneously with all incoming requests. In this version,   the   server   application is   more   complex   (since   it establishes   a   variable   number   of simultaneous   TCP   connections, one   for   each   simultaneous request) but   the   server   has   better performance (the server response time is minimized when the incoming requestsrate grows).

Implementation
--------------
TCP/UDP Sockets in Java.